require 'bundler'
require 'rake/clean'
require "rake_compiler_dock"
require 'rspec/core/rake_task'
require 'rubocop/rake_task'
require 'rake/extensiontask'
require 'gem_publisher'
require 'rb_sys'

ruby_cc_version = RakeCompilerDock.ruby_cc_version(">= 3.1")
cross_platforms = %w[
  aarch64-linux-gnu
  aarch64-linux-musl
  arm64-darwin
  x86_64-darwin
  x86_64-linux-gnu
  x86_64-linux-musl
]

CLEAN.include FileList['**/*{.o,.so,.dylib,.bundle}'],
              FileList['**/extconf.h'],
              FileList['**/Makefile'],
              FileList['pkg/']

desc 'Default: run specs'
task default: [:spec]

# test alias
task test: :spec

desc 'Run specs'
RSpec::Core::RakeTask.new do |t|
  t.rspec_opts = '--require ./spec/spec_helper.rb'
end

desc 'Lint code'
RuboCop::RakeTask.new

Bundler::GemHelper.install_tasks

desc 'Publish gem to RubyGems.org'
task :publish_gem do |_t|
  gem = GemPublisher.publish_if_updated('prometheus-client-mmap.gemspec', :rubygems)
  puts "Published #{gem}" if gem
end

task :console do
  exec 'irb -r prometheus -I ./lib'
end

gemspec = Gem::Specification.load(File.expand_path('../prometheus-client-mmap.gemspec', __FILE__))

Gem::PackageTask.new(gemspec)

Rake::ExtensionTask.new('fast_mmaped_file_rs', gemspec) do |ext|
  ext.source_pattern = "*.{rs,toml}"
  ext.cross_compile = true
  ext.cross_platform = cross_platforms
end

namespace "gem" do
  task "prepare" do
    sh "bundle"
  end

  cross_platforms.each do |plat|
    desc "Build the native gem for #{plat}"
    task plat => "prepare" do
      ENV["RCD_IMAGE"] = "rbsys/#{plat}:#{RbSys::VERSION}"

      RakeCompilerDock.sh <<~SH, platform: plat
        bundle && \
        RUBY_CC_VERSION="#{ruby_cc_version}" \
        bundle exec rake native:#{plat} pkg/#{gemspec.full_name}-#{plat}.gem
      SH
    end
  end
end

